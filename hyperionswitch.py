#! /usr/bin/env python
import subprocess
import time


def get_current_mode():
    mode = 'kodi'
    with open(MODE_FILE, 'r') as fp:
        mode = fp.read().strip()
    return mode


def update_mode(mode):
    with open(MODE_FILE, 'w') as fp:
        fp.write(mode)


def send_alert(msg):
    subprocess.call(curl_cmd, shell=True)


def kill_hyperiond():
    kill_cmd = 'killall hyperiond'
    subprocess.call(kill_cmd, shell=True)


def switch_to_mode(mode):
    if mode == 'tv':
        print 'Switching to TV mode.'
        send_alert('Switching to TV mode.')
        config_file = TV_CONFIG_FILE
    else:
        print 'Switching to KODI mode.'
        send_alert('Switching to KODI mode.')
        config_file = KODI_CONFIG_FILE

    time.sleep(1)
    start_cmd = ('/storage/hyperion/bin/hyperiond.sh {0} > '
                 '/dev/null 2>&1 &').format(config_file)
    subprocess.call(start_cmd, shell=True)


def main():
    kill_hyperiond()
    mode = get_current_mode()
    print 'Current Mode: %s' % (mode,)
    if mode == 'kodi':
        mode = 'tv'
    else:
        mode = 'kodi'
    update_mode(mode)
    switch_to_mode(mode)


if __name__ == '__main__':
    main()
    get_current_mode()
